1. What are the 3 intentional mistakes?
---
1. Missing semicolon on line 16
2. Need to import "System" for the Console.* lines to work
3. payRate must be declared as a double while assigning it on line 35

---

2. What is the output?
---
The output is: Employee John Doe of age 35 (ID: 123456) earned 196.875

---

3. (a) What is the output?
---
The output is: Employee John Doe of age 35 (ID: 123456) earned $196.88

3. (b) What is the difference between this and the previous run?
---
The difference between this and the previous run is that the money was rounded to 2 decimal places

---

4. What is the output?
---
The output is: Employee John Doe of age 35 (ID: 123456) earned $196.88

It is the same output.

---

5. (a) What is the output?
---
The output is: Employee JOHN doe of age 35 (ID: 123456) earned $196.875

5. (b) What is the difference between this output and the previous run?
---
The difference is that the employee first name is all uppercase and last name is all lowercase. His pay is rounded to 3 decimal places.
