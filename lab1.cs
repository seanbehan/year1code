using System;
public class Lab1
{
        public static void Main()
        {
                int age;
                long idNum;
                double PayRate, hours, grossPay;
                string firstName, lastName;

                // prompt the user to enter employee's first name
                Console.Write("Enter employee's first name: ");
                firstName = Console.ReadLine();

                // prompt the user to enter employee's last name
                Console.Write("Enter employee's last name: ");
                lastName = Console.ReadLine();

                // prompt the user to enter a six digit employee's identification number
                Console.Write("Enter a six digit employee's ID: ");
                // idNum = Convert.ToInt64(Console.ReadLine());
                idNum = long.Parse(Console.ReadLine());

                // prompt the user to enter employee's age
                Console.Write("Enter employee's age: ");
                // age = Convert.ToInt32(Console.ReadLine());
                age = int.Parse(Console.ReadLine());

                // prompt the user to enter the number of hours employee worked this week
                Console.Write("Enter the number of hours employee worked this week: ");
                // hours = Convert.ToDouble(Console.ReadLine());
                hours = double.Parse(Console.ReadLine());

                // prompt the user to enter the employee's hourly pay rate
                Console.Write("Enter employee's hourly pay rate: ");
                double payRate = Convert.ToDouble(Console.ReadLine());
                // payRate = double.Parse(Console.ReadLine());

                grossPay = hours * payRate; // calculate gross pay

                // output results
                // Console.WriteLine("\nEmployee {0} {1} of age {2} (ID: {3}) earned {4:C}",
                        // firstName, lastName, age, idNum, grossPay);
                Console.WriteLine("\nEmployee {0} {1} of age {2} (ID: {3}) earned {4:C3}",
                        firstName.ToUpper(), lastName.ToLower(), age, idNum,(hours * payRate));

                Console.WriteLine("Press any key to end");
                Console.ReadLine();
        }
}
