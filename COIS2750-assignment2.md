Assignment 2

1.
        a. original number    : 4 5 5 1 2 0 2 1 4 3 7 5 1 6 6 8
           sum all odd digits: 8 + 6 + 5 + 3 + 1 + 0 + 1 + 5 = 29
           sum all even digits * 2: 8 + (10-9) + 4 + 4 + 8 + (14-9) + 2 + (12-9) = 35
           total sum: 35 + 29 = 64
           mod 10 = 4

           a valid number would be 4551 2021 4375 1664

        b. original number: 5 4 2 1 3 8 2 5 6 1 2 9 8 5 3 1
           sum of odd digits: 1 + 5 + 9 + 1 + 5 + 8 + 1 + 4 = 34
           sum of even digits * 2: (10-9) + 4 + 6 + 4 + (12-9) + 4 + (16-9) + 6 = 35
           total sum: 34 + 35 = 69
           mod 10 = 9

           a valid number would be 5421 3825 6129 8532

        c. original number: 4 6 1 2 1 1 2 3 8 7 2 9 8 7 1 5
           sum of odd digits: 5 + 7 + 9 + 7 + 3 + 1 + 2 + 6 = 40
           sum of even digits * 2: 8 + 2 + 2 + 4 + (16-9) + 4 + (16-9) + 2 = 36
           total sum: 40 + 36 = 76
           mod 10 = 6

           a valid number would be 4612 1123 8729 8719

        d. original number: 6 4 3 2 1 0 9 8 7 6 5 3 2 1 0 9
           sum of odd digits: 9 + 1 + 3 + 6 + 8 + 0 + 2 + 4 = 33
           sum of even digits * 2: (12-9) + 6 + 2 + (18-9) + (14-9) + (10-9) + 4 + 0 = 30
           total sum: 63
           mod 10 = 3

           a valid number would be 6432 1098 7653 2106

2.

a.

This is what I see:

---------

Domain name	forces.ca
Domain status	auto-renew grace
Creation date	2001/02/22
Expiry date	2018/02/22
Updated date	2017/02/22
DNSSEC	Unsigned

Registrar:
Name	Internic.ca Inc.
Number	29

Registrant:
Name	Department of National Defence

Administrative contact:
Name	Jim Malone
Postal address	350 King Edward Avenue 2nd Floor South
Ottawa ON K1A0X1 Canada
Phone	+1.6139477833
Fax:
Email	jim.malone@canada.ca

Technical contact:
Name	Jim Malone
Postal address	350 King Edward Avenue 2nd Floor South
Ottawa ON K1A0X1 Canada
Phone	+1.6139477833
Fax:
Email	jim.malone@canada.ca

Name servers:
ns1.gpnet.dnd.ca 131.137.255.2
ns2.gpnet.dnd.ca  131.137.252.252

% WHOIS look-up made at 2017-03-28 01:58:22 (GMT)
%
% Use of CIRA's WHOIS service is governed by the Terms of Use in its Legal
% Notice, available at http://www.cira.ca/legal-notice/?lang=en
%
% (c) 2017 Canadian Internet Registration Authority, (http://www.cira.ca/)

--------

This is the WHOIS data for the domain forces.ca. It contains the owner's
address, phone number and when the domain registration expires.

Good guys could use this information if they needed to report a vulnerability in
the site to it's owner directly or to get in contact with the owner of the
domain for whatever reason.

Bad guys could use this information to do things like try and register the
domain themselves when it expires, or use the owners personal information for
malicious purposes like identity theft.

b.

This is what I see:

---------

--- PING mit.edu (23.38.224.128) 56(84) bytes of data. ---
64 bytes from 23.38.224.128: icmp_seq=1 ttl=53 time=175 ms
64 bytes from 23.38.224.128: icmp_seq=2 ttl=53 time=179 ms
64 bytes from 23.38.224.128: icmp_seq=3 ttl=53 time=175 ms
64 bytes from 23.38.224.128: icmp_seq=4 ttl=53 time=175 ms


--- mit.edu ping statistics ---
packets transmitted 	4
received 	4
packet loss 	0 %
time 	3004 ms


--- Round Trip Time (rtt) ---
min 	175.568 ms
avg 	176.606 ms
max 	179.211 ms
mdev 	1.508 ms

----------

This is the information on the response time of the host. It tells me that it
took on average 175 milliseconds to hit the host and for them to send a reply
packet (TCP).

Good guys can use this data to check if the host is online and accessable, and
test it's response time for network performance.

Bad guys can use ping to DDoS the host by sending lots and lots of packets that
the host isn't able to handle.


c.

I see all the servers that the packets had to go through to be able to hit the
host located at www.ox.ac.uk. The only thing I see that is a bit suprising is
that it hit the oxford servers 4 or more times before hitting the actual server.

d.

United States
IP: 54.240.242.9
City: Washington, US

Europe
IP: 52.95.217.35
City: Washington, US

Asia/Pacific
The traceroute for Asia/Pacific did not hit any IP addresses that were shown in
the traceroute.

e.

The same route is taken again, however in this case it is because the site
caches the traceroute. I know this because the response times are the exact same
on each test.

In a real traceroute test the same path is taken each time once an efficient
route has been decided. The privacy issues of this are that by making the same
route the nodes in the middle are able to guess which site is being accessed by
which computer. Also each host in between the client and the destination is able
to see the ip address of the client and all the information being sent.

3.
a.
The first 10 hex digits I see are 7b 5c 72 74 66 31 5c 61 6e 73

b.
The first 10 hex digits I see are 25 50 44 46 2d 31 2e 32 0a 25
The trailer for pdf files in hex is 25 25 45 4f 46 0a

c.
I don't see a signature in hex for this type of file, however the trailer for
the file seems to be "0a"

d.
There are 10 digits in the hex checksum, The first 7 are 2661368. There are
40 digits in the checksum, the first 7 are d754cef.

e.
The checksum's changed completely, they are almost nothing alike.

The first 7 digits of the CRC checksum of the original file are 2661368 while
the first 7 digits of the CRC checksum of the modified file are 1976695.

The first 7 digits of the RIPEMD checksum of the original file are d754cef
while the first 7 digits of the RIPEMD checksum of the modified file are
60aea8d.

f.
Changing the filename of the file has no effect on it's checksum, as the
contents of the file are not being changed in any way. The checksums are the
same as the ones from part d.

g.
It won't open because the JPEG signature has been modified in the first 4 hex
digits. To get it to open I modified these digits to change them back to what a
standard jpeg signature should look like "FF D8 FF E1". The file then opened,
The picture is of a bunch of people taking pictures of fireworks on their
cellphones.

h. Comparing the file to the other word file in this assignment, it has the
exact same signature. The reason the file doesn't open is because it's not a pdf
file. It's a word doc file. By changing the file extention from pdf to doc the
file opens fine in word. The file contains no text and a picture of a transport
truck in a snowstorm.

4.
a.
Someone who might want to steal human resources payroll information might want
to be illegally accessed by criminals looking to steal financial information
either by using identity theft or by stealing the financial information itself.

Parking permit information might want to be illegally accessed by criminals
looking to steal your identity by using information like your name and license
plate.

Counseling centre information might want to be illegally accessed by someone
wanting to steal personal information for purposes like blackmail or identity
theft.

b.
The impact of human resources payroll being accessed or damaged would be
catastrophic, as financial data such as people's SSNs would be lost or stolen.
This data can't be recreated and would cause the school to get in lots of
lawsuits with individuals. The school would lose lots of reputation for losing
the financial data.

The impact of parking permit information being accessed or damaged is no big
deal really, the only information that would be lost would be names and license
plate numbers. This isn't enough to build a full profile on an individual, and
the data could easily be recreated using financial data.

The impact of counseling information being accessed or damaged would be serious.
Trent would lose reputation for losing people's personal information, and it
could be used to create a profile for identity theft. It has the potential for
lawsuits, but none that would be overly major. The data would be expensive to
recreate because you would have to get it from all the patients again.

c.
I think the likelihood of the human resources payroll being accessed or damaged
would be very likely, as it has things like financial data that criminals might
want to access. This means it is more likely going to be a target for criminals.

I think the likelihood of the parking permit information being accessed or
damaged would be not likely. I don't think parking permit information would seem
like an interesting thing to steal for criminals, as it wouldn't contain much
useful info and wouldn't cause much damage if destroyed.

I think the likelihood of the counseling information being accessed or damaged
would be moderately likely, as there could be some interesting information in
there for criminals to steal or destroy. It would be very useful for identity
theft or blackmail.

However I don't know the configuration and can't comment on how easy it would be
for them to access the data, as I don't know how it's configured. This is a big
factor on how easily accessible any system is for criminals.

d.
The best way to manage the risk for the human resources is by avoiding it, as it
would be catastrophic for all the payroll SSNs to be leaked to criminals. One
way of avoiding it is keeping the payroll completely local and offline on local
machines that are safeguarded and only accessible by privileged individuals.

The best way to manage risk for parking permit information is to transfer the
risk to others. Parking permit information isn't very important and shouldn't be
bothered with very much. The risk could be passed on to individuals instead of
being managed by the school.

The best way to manage risk for counseling center information is to avoid the
risk, losing psychological counseling information would be very bad and could
result in lots of lawsuits. The systems containing this information should be
hardened and kept up to date as much as possible to prevent the possibility of
exploits.

5.
https://www.forbes.com/sites/laurashin/2016/12/20/hackers-have-stolen-millions-of-dollars-in-bitcoin-using-only-phone-numbers/

In this example a social engineering attack was performed on Jered Kenna's
company. The attack consisted of multiple layers of security since Jered
actually knew what he was doing. He had his companies assets (millions of
dollars in bitcoin) stored on an encrypted hard drive that was almost always
kept offline. However recently he moved the bitcoin to online bitcoin services.
The way he was hacked was as such, first the hacker used a social engineering
attack on t-mobile to steal his phone number and transfer it to another third
party cellular provider. Next the hacker used the stolen number to send himself
Jered's 2-step verification code for his email. Once the hacker had access to
his email they essentially had already pwned him. Within 7 minutes the hacker
was able to access all of his accounts and had accessed his bitcoin and
transferred all of it away. The nature of this attack was criminal, as the
intent was to steal Jered's companies assets. It is unknown who perpetrated the
attack. The cost of this attack consisted of the loss of over 1 million US
dollars.

The lessons that can be learned from this attack are that you shouldn't enable
receiving 2-step verification codes by SMS.

https://www.wired.com/2016/06/hey-stop-using-texts-two-factor-authentication/

According to Wired, they are often the "weakest link" in 2-step as like in this
example the cell provider can be easily social engineered. If Jered had disabled
SMS codes for 2-step verification he would have been safe from the attack that
the hacker performed.

Another lesson that can be learned is not to use online bitcoin providers.

https://www.coinmanual.com/bitcoin-wallets-offline-vs-online/

Online providers are convenient, but for how much bitcoin Jered had keeping them
all on the internet was a very bad idea because with access to his email and
phone he was able to get access into each one of them. Local wallets are much
more secure if kept safe.

Another lesson that can be learned is not to keep all your eggs in one basket.

https://blog.cex.io/bitcoin-talks/5-tips-how-not-to-lose-your-bitcoins-12258

If Jered had separated his bitcoin's onto different offline drives a lot of them
would have been safe from this sort of attack. The loss wouldn't have been
nearly as catastrophic. This would have prevented the hacker from stealing all
of his coins, as a lot of them would have been safe on offline drives.
