Ransomware has a negative impact on society as it affects users financially. At Carlton University "attackers are asking for either two bitcoin per machine, or 39 bitcoin total to release the encrypted files — the latter equalling nearly $38,941 at today's rate on the popular Bitcoin exchange Coinbase." (Braga, Matthew) This is a crazy sum of money that most cannot afford to be handing over to hackers just to recieve files that should be rightfully their property.

Recently "New ransomware lets you decrypt your files — by infecting other users". "let victims unlock their files the "nasty way" by sharing a link with two other people -- presumably ones the victim doesn't like. If they become infected and pay, then the original victim will receive a free decryption key." This is ideal for the attackers, as for each user who shares an infected link with friends they're affectively growing at a rate of n^2.

Another example of how ransomware is evolving is "New Ransomware CryptoFortress Encrypts Unmapped Network Shares". This means that drives that you have access to over a network will be infected with the ransomware as well. This is interesting because it can affect not only the infected computer, but all other computers which use this drive. I don't believe that this malware can do this, but it would be interesting to see if the ransomware would be able to spread across the network by infecting other devices that are connected to the network drive.

The role of the computer forensic analyst in the case of ransomware is to prevent it before it happens. As once ransomware is on a computer it will be near impossible to remove without purchasing the decryption keys. The only thing that a forensic analyst can possibly do after a machine has been infected would be to find out how it happened and to prevent it from happening next time.

What steps do you think you can take to protect yourself or others from ransomware?

Braga, Matthew. "Carleton University Computers Being Held Hostage for Bitcoin." CBCnews. CBC/Radio Canada, 29 Nov. 2016. Web. 15 Jan. 2017.

Whittaker, Zack. "New Ransomware Lets You Unlock Your Files - by Infecting Your Friends." ZDNet. N.p., 12 Dec. 2016. Web. 15 Jan. 2017.

Sjouwerman, Stu. "New Ransomware CryptoFortress Encrypts Unmapped Network Shares." New Ransomware CryptoFortress Encrypts Unmapped Network Shares. KnowBe4, n.d. Web. 15 Jan. 2017.
