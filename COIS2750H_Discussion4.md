Hey Sarah,

I think that the reason there isn't a larger education piece devoted to specific
demographics who are at a higher vulnerability to scams is because it would be
very expensive, it takes a lot of time, and most of the demographic who is at
risk would probably not want to go through cyber scam education. There are also
a lot of people in this demographic.

The cost to educate all of these people would be very expensive for governments
just for scammers to step up their game. I think that finding the scammers
behind the attacks and adding countermeasures such as phishing detection in
browsers and emails would be more worthwhile. It would be good if companies
could make it clear that they will never ask for certain info over the phone or
email to protect their customers, and some already do.

There are too many people in this demographic to educate them all. There are
at millions of people in the age range of 60+ in Canada, you can't just educate
all of those people all at once. Especially when most of them probably don't
want to be educated or would think it's a waste of their time. Not all of these
people even use computers or have to worry about being scammed because they
don't manage their own finances.

A better way to stop scammers from scamming would be to make more secure forms
of communication. Both Email and Phone calls are the most commonly used mediums
for scammers, neither of these mediums have verified senders/callers. This is a
big part of the problem. There is nothing in the way of scammers doing their
scams other than software on the receiving side. There is also not much being
done to stop scammers from obtaining and spoofing email addresses. I think that
the system itself is broken.

As an alternative to email and traditional phone calls I would recommend that
users start using something such as Signal. Signal is a messaging platform which
supports IM, calling, and video calling. All messages that are sent are
encrypted and verified to be valid to be who they say they're from. This would
solve most of the issues with phishing scams, which are very common.

"The Canadian Population in 2011: Age and Sex." Census Program. N.p., 21 Dec. 2015. Web. 14 Mar. 2017.
