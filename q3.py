def cut_spaces(myStr):
    if myStr[0] == ' ':
        myStr = myStr[1:]
    if myStr[:len(myStr)] == ' ':
        myStr = myStr[:len(myStr)-1]
    return myStr

def get_code(scode, passphrase):
    code = ['', '']
    passphrase = passphrase.lower()
    scode = scode.lower()
    scode = scode.split(' ')
    for pair_count, pair in enumerate(scode, 0):
        for letter in pair:
            for char_count, char in enumerate(passphrase, 0):
                if letter == char:
                    code[pair_count] += str(char_count)
    return code

def rot(n, char, alphabet):
    rotated = ''
    for count, letter in enumerate(alphabet, 0):
        if char == letter:
            if (count + n) < len(alphabet):
                rotated = alphabet[count+n]
            else:
                rotn = n - (len(alphabet) - count)
                rotated = alphabet[rotn]
    return rotated

def rotate(message, code, alphabet):
    rotated = ''
    print(code)
    for i, pair in enumerate(code, 0):
        n = int(code[i][1])
        length = int(code[i][0])
        for rotation in range(length):
            for char in message:
                rotated += rot(n, char, alphabet)
    return rotated

def get_cut(message): ### FIX
    count = 0
    for char_count, char in enumerate(message, 0):
        if char == ' ':
            count = 0
        else:
            if count > 1:
                break
            count += 1
    return char_count - 3 # subtract 3 because it went 3 over to discover that it wasn't part of the scode

def decrypt(message, passphrase, alphabet):
    cut = get_cut(message)
    print(cut)
    scode = message[:cut]
    message = cut_spaces(message[cut:])
    code = cut_spaces(get_code(scode, passphrase))
    decrypted_message = rotate(message, code, alphabet)
    return decrypted_message

'''
Let’s look at an old Roman age encryption scheme. Let’s say we intercepted a message from a known Celtic hacker group. We know from experience that this group uses the characters from A to Z, then a space, and then the numerals from 0 to 9 and employs a wrap around (moving left from A gives us 9). The first leading pairs of letters tell us what the substitution code is using the code phrase “Trudy Jones”.
For example if the first two pairs are uy du the code is 24 and 32. This would mean that we move the first 2 letters 4 places to the right eg. A becomes E), and then the next 3 letters 2 places to the left (eg. A becomes 8), then 4 places to the right for the next 2 characters and so on to encrypt the message.
'''

message = 'dj ud YMJXDWJFQXTSJ'
passphrase = 'Trudy Jones'
alphabet ='ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789'

print(decrypt(message, passphrase, alphabet))
