using System;

/* Sean Behan, 0599444, Program to input a double array and compute statistics */

/*
 * array: holds the array
 * user_input: holds the user input to see if they want to exit or not
 * no_exit: boolean value set to false when the user wants to exit so the loop can exit
 * n: holds the number of values the user wants in their array, to a max of 20 elements
 * average: holds the average of the array
 * min: holds the max of the array
 * max: holds the min of the array
 */

class COIS1020Assingment1q7 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0.
         */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }


        public static void InputArray(double[] array, ref int n) {
        // Method:      InputArray
        // Description: Fill an array with values.
        // Arguments:   array: the array to fill.
        //              n: number of elements in the array.
        // Return:      void
                int i = 0;
                while ((n >= 21) || (n < 0))
                {
                        Console.Write("Enter the number of elements: ");
                        n = ConvertToInt(Console.ReadLine());
                }
                Console.WriteLine("Enter the {0} elements:", n);
                for(i = 0; i < n; ++i)
                        array[i] = ConvertToInt(Console.ReadLine());
        }

        public static double ComputeStats(double[] array, int n, ref double min, ref double max) {
                double average = 0;
                int i = 0;
                double sum = 0;
                min = array[0]; // set min to the first value in the array
                max = array[0]; // set max to the first value in the array
                for(i = 0; i < n; ++i) // loop through the array and get the sum, min, and max
                {
                        sum += array[i];
                        if (array[i] < min) // check if it's smaller and if it is, save it to min
                        {
                                min = array[i];
                        }
                        if (array[i] > max) // check if it's larger and if it is, save it to max
                        {
                                max = array[i];
                        }
                }
                average = sum / n; // use the sum to calculate the average
                return average;
        }


        public static void Main() {
                char user_input = 'a';
                bool no_exit = true;
                double[] array = new double[20];
                int n = -1;
                double average, min, max;
                average = 0;
                min = 0;
                max = 0;


                while (no_exit)
                {

                        InputArray(array, ref n); // user input's array
                        average = ComputeStats(array, n, ref min, ref max); // compute information about the array
                        Console.WriteLine("The average of the array is {0}", average); // show the user the computed values
                        Console.WriteLine("The minimum value of the array is {0}", min);
                        Console.WriteLine("The maximum of the array is {0}", max);
                        Console.Write("Enter another array? Q to quit: ");
                        try  // parse input to make sure it is sane
                        {
                                user_input = Char.ToLower(Console.ReadLine()[0]);  // prompt user for which option
                        }
                        catch{}  // we don't really need to check it, the switch statement will do it for us when it loops over
                        if(user_input == 'q') // ask the user if they want to exit
                        {
                                no_exit = false;
                        }
                        n = -1; // reinitialize n
                }
        }
}
