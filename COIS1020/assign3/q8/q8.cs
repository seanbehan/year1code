using System;

/* Sean Behan, 0599444, Program to input a circle object and display it's area and circumference */

/*
 * radius: holds the circle radius
 * user_input: holds the user input to see if they want to exit or not
 * no_exit: boolean value set to false when the user wants to exit so the loop can exit
 */

public class Circle
{
        public static double radius;
        public static double Radius {
                get {return radius;}
                set
                {
                        if (radius < 0)
                        {
                                radius = 0;
                        }
                        else
                        {
                                radius = value;
                        }
                }
        }

        public const double PI = 3.14159;

        public Circle(double r) {
                radius = r;
        }

        public Circle() {
                radius = 0.0;
        }

        public static double GetArea (){
                return (PI * (Math.Pow(radius, 2))); // pi * (r^2)
        }

        public static double GetCircumference() {
                return (2*(PI * radius)); // 2*(pi*r)
        }
}


class COIS1020Assingment1q8 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0.
         */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }


        public static void Main() {
                bool no_exit = true;
                char user_input = 'a';

                while (no_exit)
                {
                        Console.Write("Enter the radius of the circle: ");
                        new Circle(double.Parse(Console.ReadLine()));
                        if (Circle.Radius <= 0)
                        {
                                Console.WriteLine("Invalid radius size.");
                        }
                        else
                        {
                                Console.WriteLine("The Area of the circle is: {0}", Circle.GetArea());
                                Console.WriteLine("The Circumference of the circle is: {0}", Circle.GetCircumference());
                        }
                        Console.Write("Enter another circle? Q to quit: ");
                        try  // parse input to make sure it is sane
                        {
                                user_input = Char.ToLower(Console.ReadLine()[0]);  // prompt user for which option
                        }
                        catch{}  // we don't really need to check it, the switch statement will do it for us when it loops over
                        if(user_input == 'q') // ask the user if they want to exit
                        {
                                no_exit = false;
                        }
                }
        }
}
