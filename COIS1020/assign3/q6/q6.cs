using System;

/* Sean Behan, 0599444, Program that manages someones bank balance */

/*
 * balance: holds the user's account balance
 * no_exit checks holds a boolean value to check if we're supposted to exit at the bottom of the loop
 * user_input holds the user input for c, d, p, or q
 * user_money_input holds the user input for amount of money they want to deposit or cheque
 */

class COIS1020Assingment1q6 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0.
         */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }


        public static void InsufficientFunds() {
                Console.WriteLine("There are insufficient funds to do the action you requested.");
        }


        public static double NonNegInput() {
                /* checks if the output is negative and keeps looping until they enter a valid value */
                bool no_exit = true;
                double x = 0; // used to hold the user input if valid

                while (no_exit)
                {
                        Console.Write("Enter how much $: ");
                        Double.TryParse(Console.ReadLine(), out x);  // input money and parse it as a double
                        if (x > 0)  // if the input is valid we tell the loop to exit
                        {
                                no_exit = false;
                        }
                }
                return x;
        }


        public static void Cheque(double amount, ref double balance) {
                double fee = 2.25;

                if (((balance - amount) - fee) >= 0) // check if there are enough funds
                {
                        balance = (balance - amount) - fee;
                }
                else
                {
                        InsufficientFunds();
                }
        }


        public static void Deposit(double amount, ref double balance) {
                balance += amount; // add the amount to the balance
        }


        public static void Print(double balance) {
                Console.WriteLine("Your balance is {0:C2}", balance); // show the user their balance
        }


        public static void Main() {
                double balance = 0;
                bool no_exit = true;
                char user_input = 'a';
                double user_money_input;

                do
                {
                        Console.WriteLine("\nC for Cheque"); // show the user the options they have
                        Console.WriteLine("D for Deposit");
                        Console.WriteLine("P for Print");
                        Console.WriteLine("Q for Quit");
                        Console.Write("Enter an option: ");
                        try  // parse input to make sure it is sane
                        {
                                user_input = Char.ToLower(Console.ReadLine()[0]);  // prompt user for which option
                        }
                        catch{}  // we don't really need to check it, the switch statement will do it for us when it loops over

                        switch (user_input)  // check input
                        {
                                case 'c': // chequeing
                                        user_money_input = NonNegInput();
                                        Cheque(user_money_input, ref balance);
                                        break;
                                case 'd': // deposit
                                        user_money_input = NonNegInput();
                                        Deposit(user_money_input, ref balance);
                                        break;
                                case 'p': // print
                                        Print(balance);
                                        break;
                                case 'q': // quit
                                        no_exit = false; // set the loop variable to exit
                                        break;
                        }
                } while (no_exit);
                Console.WriteLine("Exiting..."); // tell the user we're exiting
        }
}
