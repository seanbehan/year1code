using System;
public static class Lab2
{
public static void Main()
        {
        int sum = 0;
        int count = 0;
        int value = 0;
        double average = 0.0;

        while (value >= 0)
        {
                // prompt the user to enter an integer
                Console.Write("Enter a positive integer (-1 to stop): ");
                value = Convert.ToInt32(Console.ReadLine());

                sum += value;
                count++;
        }

        // Calculate average
        if (sum == 0)
                average = 0.0;
        else
                average = sum / (double)count;

        // Print results
        Console.WriteLine("sum = {0} ", sum);
        Console.WriteLine("average = {0:F2} ", average);

        // Pause until user is done
        Console.WriteLine("Press any key to end");
        Console.ReadLine();
        }
}
