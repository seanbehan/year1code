using System;

/* Sean Behan, 0599444, Program that computes the charge for a Rick's Motorlodge room */

/*
 * userName holds the users name
 * roomType holds the type of room
 * numDays  holds the number of days the user wants to stay
 * loopContinue1 variable to decide wheter to continue the loop or not
 * loopContinue variable to decide wheter to continue the loop or not
 * roomCharge holds the cost that the room will come to
 */

class COIS1020Assingment1q5 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0. */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }

        public static double RoomCharge(int numDays, char roomType) {
        /* Takes the days and room type and calculates the cost
         *
         * :numDays: the number of days to stay
         * :roomType: the type of room to book
         * :returns: the cost of the room
         */
                double hst = 1.13;
                int doubleCost = 80;
                int kingCost = 110;
                int suiteCost = 150;
                double cost = 0;

                // calculate cost based on roomType
                switch (roomType)
                {
                        case 'D':
                                cost = doubleCost * numDays * hst;
                                break;
                        case 'K':
                                cost = kingCost * numDays * hst;
                                break;
                        case 'S':
                                cost = suiteCost * numDays * hst;
                                break;
                }
                return cost;
        }

        public static void Main() {
                string userName, roomTypeStr;
                char roomType = 'D';
                int numDays = 0;
                bool loopContinue1 = true, loopContinue = true;
                double roomCharge = 0;

                while(loopContinue1) // loop until the user wants to exit
                {
                        // ask the user for their name, store it
                        Console.Write("Enter name: ");
                        userName = Console.ReadLine();

                        // show the user types of rooms
                        Console.WriteLine("Room types:");
                        Console.WriteLine("D: Double");
                        Console.WriteLine("K: King");
                        Console.WriteLine("S: Suite");

                        while(loopContinue) // loop to validate user input
                        {
                                Console.Write("Enter your room type: ");
                                roomTypeStr = Console.ReadLine().ToUpper();
                                roomType = roomTypeStr[0];
                                if((roomType == 'D') || (roomType == 'K') || (roomType =='S'))
                                {
                                        loopContinue = false;
                                }
                        }

                        loopContinue = true;
                        while(loopContinue) // loop to validate user input
                        {
                                Console.Write("Enter number of days: ");
                                numDays = ConvertToInt(Console.ReadLine());
                                if(numDays > 0)
                                {
                                        loopContinue = false;
                                }
                        }

                        // calculate roomCharge using the RoomCharge function, save it in roomCharge
                        roomCharge = RoomCharge(numDays, roomType);

                        // display the cost of the room to the user
                        Console.WriteLine("Thank you {0}, this room will cost you {1:C2}.", userName, roomCharge);

                        // ask the user if they want to continue, if they don't the loop exits
                        Console.WriteLine("To continue press any key");
                        Console.WriteLine("To exit press K...");
                        if (Console.ReadLine().ToUpper() == "K")
                        {
                                loopContinue1 = false;
                        }
                }
        }
}
