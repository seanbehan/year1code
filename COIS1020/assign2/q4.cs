using System;

/* Sean Behan, 0599444, Program that determines the GCD of two integer numbers */

/*
 * number1, number2: hold user inputted numbers
 * gcd: holds the gcd of the two numbers
 * largerNum: holds the larger of the two numbers
 * smallerNum: holds the smaller of the two numbers
 * loopContinue: tells the loop to stop when its set to false on valid user input
 */

class COIS1020Assingment1q4 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0. */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }

        public static int GCDofNumbers(int number1, int number2)
        /* Takes 2 numbers and returns the GCD of them
         *
         * :number1: first number for gcd 
         * :number2: second number for gcd
         * :returns: the gcd of number1 and number2
         */
        {
                int gcd = 0, largerNum, smallerNum;

                if(number1 > number2)
                {
                        largerNum = number1;
                        smallerNum = number2;
                }
                else // if they're equal it doesnt matter, so we just set them opposite
                     // for both the equal condition and the less than condition
                {
                        largerNum = number2;
                        smallerNum = number1;
                }

                for(int i = 1; i <= smallerNum; i++)
                {
                        if((smallerNum % i == 0) && (largerNum % i == 0))
                        {
                                // Console.Write("GCD set to {0}", i);
                                gcd = i;
                        }
                }
                return gcd;
        }

        public static void Main() {
                int number1 = 0, number2 = 0, gcd = 0;
                bool loopContinue = true;

                // loop to check for valid input from the user
                while(loopContinue)
                {
                        Console.Write("Enter the first positive number: ");
                        number1 = ConvertToInt(Console.ReadLine());
                        if(number1 > 0)
                        {
                                loopContinue = false;
                        }
                }

                loopContinue = true; // reset loop checking variable for the next loop
                // loop to check for valid input from the user
                while(loopContinue)
                {
                        Console.Write("Enter the second positive number: ");
                        number2 = ConvertToInt(Console.ReadLine());
                        if(number2 > 0)
                        {
                                loopContinue = false;
                        }
                }

                gcd = GCDofNumbers(number1, number2);

                // for whatever reason, if the gcd comes back as 0, it will display this
                if(gcd == 0)
                {
                        Console.WriteLine("The two numbers do not have a GCD.");
                }
                else // otherwise, show the user the GCD that was calculated
                {
                        Console.WriteLine("The two numbers GCD is {0}.", gcd);
                }

                Console.ReadLine(); // so the program doesn't exit
        }
}
