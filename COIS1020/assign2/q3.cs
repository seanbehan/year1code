using System;

/* Sean Behan, 0599444, Program that determines the ESP ability of a user  */

/*
 * rnd: holds the random number list
 * randint: holds a random number between 1 and 10
 * guessCount: holds the number of guesses
 * userGuess: holds the users guess
 * incorrectGuess: holds a boolean value to show when to leave the loop
 */

class COIS1020Assingment1q3 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0. */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }

        public static void Main() {
                Random rnd = new Random();
                int randint = rnd.Next(1,11); // get a random number between 1 and 10 and store it in randint
                int userGuess;
                int guessCount = 1;
                bool incorrectGuess = true;

                while(incorrectGuess) // loop to continue until the user gives valid input
                {
                        // user inputs their guess
                        Console.Write("Enter a guess between 1 and 10: ");
                        userGuess = ConvertToInt(Console.ReadLine());

                        // if the guess is correct, set the variable to stop looping
                        if(userGuess == randint)
                        {
                                incorrectGuess = false;
                        }
                        // if they didn't guess valid input, don't add it to the guess count
                        else if ((userGuess < 11) && (userGuess > 0))
                        {
                                guessCount += 1;
                        }
                }

                // when the user guessed correctly the previous loop exits
                // this tells the user their score
                Console.WriteLine("Congradulations, you guessed the number correctly in {0} guesses.\nThe number was {1}.", guessCount, randint);
                if (guessCount >= 5)
                {
                        Console.WriteLine("Stay in school");
                }
                else if (guessCount >= 3)
                {
                        Console.WriteLine("You have potential");
                }
                else if (guessCount >= 1)
                {
                        Console.WriteLine("You are psychic!");
                }

                Console.ReadLine(); // so the program doesn't exit
        }
}
