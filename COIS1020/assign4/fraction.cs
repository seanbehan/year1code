using System;
namespace COIS1020Assignment4_q9
{
        public class Fraction
        {
                private int numerator;
                private int denominator;

                // Constructor (no argument)
                public Fraction()
                {
                        numerator = 0;
                        denominator = 1;
                }

                // Constructor
                public Fraction(int num, int den)
                {
                        numerator = num;
                        denominator = den;
                        Reduce(); // reduce to lowest terms
                }

                // Numerator Property
                public int Numerator
                {
                        get { return numerator; }
                        set { numerator = value; }

                }

                // Denominator Property
                public int Denominator
                {
                        get { return denominator; }
                        set { denominator = value; }
                }

                //Reduce method
                private void Reduce()
                /* Takes 2 numbers and returns the GCD of them
                 *
                 * :numerator: first number for gcd
                 * :denominator: second number for gcd
                 * :returns: the gcd of numerator and denominator
                 */
                {
                        int gcd = 0, largerNum, smallerNum;

                        if(numerator > denominator)
                        {
                                largerNum = numerator;
                                smallerNum = denominator;
                        }
                        else // if they're equal it doesnt matter, so we just set them opposite
                             // for both the equal condition and the less than condition
                        {
                                largerNum = denominator;
                                smallerNum = numerator;
                        }

                        for(int i = 1; i <= smallerNum; i++)
                        {
                                if((smallerNum % i == 0) && (largerNum % i == 0))
                                {
                                        // Console.Write("GCD set to {0}", i);
                                        gcd = i;
                                }
                        }

                        if (gcd > 1) // reduce the fraction if it can be reduced
                        {
                                numerator = numerator / gcd;
                                denominator = denominator / gcd;
                        }
                }

                // ToString method
                public override string ToString()
                {
                        return numerator.ToString() + "/" + denominator.ToString();
                }

                // Multiply method
                public static Fraction operator*(Fraction fract1, Fraction fract2)
                {
                        Fraction fract3 = new Fraction();
                        fract3.Numerator = fract1.Numerator * fract2.Numerator;
                        fract3.Denominator = fract1.Denominator * fract2.Denominator;
                        fract3.Reduce();

                        return fract3;
                }

                // Greater Than method
                public static bool operator>(Fraction fract1, Fraction fract2)
                {
                        double realvalue1, realvalue2;
                        realvalue1 = (double) fract1.Numerator / fract1.Denominator;
                        realvalue2 = (double) fract2.Numerator / fract2.Denominator;

                        if (realvalue1 > realvalue2)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }

                // Less Than method
                public static bool operator<(Fraction fract1, Fraction fract2)
                {
                        double realvalue1, realvalue2;
                        realvalue1 = (double) fract1.Numerator / fract1.Denominator;
                        realvalue2 = (double) fract2.Numerator / fract2.Denominator;

                        if (realvalue1 < realvalue2)
                        {
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
        }
}
