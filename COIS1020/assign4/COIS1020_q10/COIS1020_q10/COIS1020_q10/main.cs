﻿using System;

/* Sean Behan, 0599444, Program to input a number and display it's square root */

/*
 * number: holds the user input
 * result: holds the result of the SquareRoot
 * keep_asking: holds a boolean value for whether the loop should continue asking the user
 */

namespace COIS1020_q10
{
        class COIS1020_q10
        {
                public static double SquareRoot(double value){
                        if (value < 0) // if it's a negative number we throw an exception
                                throw new ApplicationException();
                        return Math.Sqrt(value);
                }

                public static void Main(){
                        double number = 0; // holds the user input
                        double result = 0; // holds the result of the SquareRoot
                        bool keep_asking = true; // holds a boolean value for whether the loop should continue asking the user

                        while(keep_asking)
                        {
                                Console.Write("Enter a number to get the square root of: ");
                                // ask the user which number they want to square root

                                try
                                {
                                        number = Convert.ToDouble(Console.ReadLine()); // try to convert it to a double
                                        keep_asking = false; // tell the loop to pass
                                }
                                catch(FormatException) // if it can't be converted to a double we throw a format exception
                                {
                                        Console.WriteLine("ERROR: Please enter a number\n");
                                        keep_asking = true; // tell the loop to continue
                                        continue; // don't bother trying to square root if it's not a number, go to the next iteration
                                }

                                try
                                {
                                        result = SquareRoot(number); // try to square root the number
                                        keep_asking = false; // tell the loop to pass
                                }
                                catch(ApplicationException) // catch the exception if it's a negative number, and can't be square rooted
                                {
                                        Console.WriteLine("ERROR: Please enter a positive number\n");
                                        keep_asking = true; // tell the loop to continue
                                }
                        }
                        Console.WriteLine("The square root of {0} is {1:0.00}", number, result); // display the resulting square root to the user

                        Console.WriteLine("\n Press any key to exit...");
                        Console.ReadKey();
                }
        }
}
