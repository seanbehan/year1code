Enter the first numerator: -1
Enter the first denominator: 2
Enter the second numerator: 2
Enter the second denominator: 3
-1/2 x 2/3 = -2/6

2/3 is larger than -1/2
-1/2 is smaller than 2/3


Enter the first numerator: 5
Enter the first denominator: 2
Enter the second numerator: 2
Enter the second denominator: 6
5/2 x 2/6 = 5/6

5/2 is larger than 2/6
2/6 is smaller than 5/2


Enter the first numerator: 5
Enter the first denominator: 10
Enter the second numerator: 1
Enter the second denominator: 2
5/10 x 1/2 = 1/4

1/2 is larger than 5/10
1/2 is smaller than 5/10
