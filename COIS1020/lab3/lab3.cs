using System;
public static class Lab3
{
        public static int Sum(ref int count, int numberOfValues)
        {
                int value = 0;
                int sum = 0;
                // Get the next value and calculate the sum
                for (count = 0; count < numberOfValues; ++count)
                {
                        // Read next value
                        value = ReadValue("Enter a positive integer: ");
                        // Calculate the sum
                        sum += value;
                }
                return sum;
        }


        public static int Sum(ref int count)
        {
                int value = 0;
                int sum = 0;
                // Read initial value
                value = ReadValue("Enter a positive integer (-1 to stop): ");
                // Calculate the sum and get the next value
                while (value >= 0)
                {
                        // Calculate the sum
                        sum += value;
                        count++;
                        // Read next value
                        value = ReadValue("Enter a positive integer (-1 to stop): ");
                }
                return sum;
        }


        public static double Average(int sum, int count)
        {
                double average = 0.0;
                if (sum == 0)
                        average = 0.0;
                else
                        average = sum / (double)count;
                return average;
        }


        public static int ReadValue(string messageToUser)
        {
                int value = 0;
                Console.Write(messageToUser);
                value = Convert.ToInt32(Console.ReadLine());
                return value;
        }


        public static void Main()
        {
                int sum = 0;
                int count = 0;
                int value = 0;
                double average = 0.0;

                // Get the sum of the values
                sum = Sum(ref count, 6);

                // Calculate average
                average = Average(sum, count);

                // Calculate average
                if (sum == 0)
                        average = 0.0;
                else
                        average = sum / (double)count;

                // Print results
                Console.WriteLine("sum = {0} ", sum);
                Console.WriteLine("average = {0:F2} ", average);

                // Pause until user is done
                Console.WriteLine("Press any key to end");
                Console.ReadLine();
        }
}
