using System;

/* Sean Behan, 0599444, Program that computes the amount of money in your piggy-bank */

/*
 * userName: holds the users name
 * coin* : holds the number of the type of coin
 * totalValue: holds the value of all the coins
 */

class COIS1020Assingment1q1 {
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0. */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }

        public static void Main() {
        string userName;
        int coinPennies, coinNickles, coinDimes, coinQuarters, coinLoonies, coinToonies;
        double totalValue;

        Console.Write("Enter your name: ");
        userName = Console.ReadLine();

        Console.WriteLine("Hello, " + userName);
        Console.WriteLine("Enter number of coins.");

        Console.Write("Pennies: ");
        coinPennies = ConvertToInt(Console.ReadLine());

        Console.Write("Nickles: ");
        coinNickles = ConvertToInt(Console.ReadLine());

        Console.Write("Dimes: ");
        coinDimes = ConvertToInt(Console.ReadLine());

        Console.Write("Quarters: ");
        coinQuarters = ConvertToInt(Console.ReadLine());

        Console.Write("Loonies: ");
        coinLoonies = ConvertToInt(Console.ReadLine());

        Console.Write("Toonies: ");
        coinToonies = ConvertToInt(Console.ReadLine());

        /* Calculate total value */
        totalValue = 0;
        for (int i = coinToonies; i > 0; i--)
        {
                totalValue += 2;
        }
        for (int i = coinLoonies; i > 0; i--)
        {
                totalValue += 1;
        }
        for (int i = coinQuarters; i > 0; i--)
        {
                totalValue += 0.25;
        }
        for (int i = coinDimes; i > 0; i--)
        {
                totalValue += 0.1;
        }
        for (int i = coinNickles; i > 0; i--)
        {
                totalValue += 0.05;
        }
        for (int i = coinPennies; i > 0; i--)
        {
                totalValue += 0.01;
        }

        Console.WriteLine("Your total is: {0:C2}", totalValue);

        Console.ReadLine(); // so the program doesn't exit
        }
}
