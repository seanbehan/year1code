using System;

/* Sean Behan, 0599444, Program that computes the cost of donuts */

/*
 * donuts: holds the number of donuts
 * costPerDonut: a variable that stores the cost per donut
 * cash: holds the value of the number of donuts
 */

class COIS1020Assingment1q2 {
        const double luxuryCost = 0.25;
        /* magic function to convert donuts into cash */
        public static double DonutToCash(int donuts) {
                double costPerDonut, cash, hst;
                cash = 0;
                costPerDonut = 0;
                hst = 1.13;
                if(donuts <= 6)
                        costPerDonut = 1;
                else if(donuts < 12)
                        costPerDonut = 0.9;
                else if(donuts >= 12)
                        costPerDonut = 0.75;
                        hst = 1;
                /*for(int i = donuts; i > 0; i--)
                {
                        cash += costPerDonut;
                }*/ // I'm actually an idiot...
                cash = costPerDonut * donuts;
                if (cash > 0)
                {
                        cash += luxuryCost;
                        cash *= hst;
                }
                return cash;

        }
        /* Function to validate user input for garbage.
         * Any input that can't be converted to an int
         * is interpereted as 0. */
        public static int ConvertToInt(string number) {
                int output = 0;
                int.TryParse(number.Trim(), out output);
                return output;
        }
        public static void Main() {
        string userName;
        int donuts;
        double cash = 0;

        Console.Write("Enter your name: ");
        userName = Console.ReadLine();

        Console.Write("Enter number of donuts: ");
        donuts = ConvertToInt(Console.ReadLine());
        cash = DonutToCash(donuts);

        Console.WriteLine("Mr/Ms {0}, your total is {1} donuts, which will cost you {2:C2}", userName, donuts, cash);

        Console.ReadLine(); // so the program doesn't exit
        }
}
