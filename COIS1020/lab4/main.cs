using System;
public static class Lab4
{
        public static void Fill(int[] B, ref int num) {
        // Method:      Fill
        // Description: Fill an array with values.
        // Arguments:   B: the array to fill.
        //              num: number of elements in the array.
        // Return:      void
                int i = 0;
                num = -1;
                while ((num >= 10) || (num < 0))
                {
                        Console.Write("Enter the number of elements: ");
                        num = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Enter the {0} elements:", num);
                for(i = 0; i < num; ++i)
                        B[i] = Convert.ToInt32(Console.ReadLine());
        }


        public static void Dump(int[] C, int num) {
        // Method:      Dump
        // Description: Display the contents of an array.
        // Arguments:   C: array to be displayed.
        //              num: number of elements in the array.
        // Return:      void
                int i = 0;
                Console.WriteLine("The Elements in the array are:");
                for (i = 0; i < num; ++i)
                        Console.WriteLine("\t{0}", C[i]);
        }


        public static void WaitForKey() {
        // Method:      WaitForKey
        // Description: Wait for a key press
        // Arguments:   None
        // Return:      void
                Console.WriteLine("Press any key to end");
                Console.ReadLine();
        }


        public static void Reverse(int[] D, int num) {
                int i;
                int j = num; // length of the array
                int x; // holds values when swapping elements
                for (i = 0; i < num / 2; i++, j--) {
                        x = D[i];
                        D[i] = D[j];
                        D[j] = x;
                }
                Console.WriteLine("The Elements in the array are:");
                for (i = 1; i <= num; ++i)
                        Console.WriteLine("\t{0}", D[i]);
        }


        public static void Compute(int[] E, int num) {
                int sum = 0;
                int i;
                double average = 0;
                for (i = 0; i < num; ++i)
                {
                        sum += E[i];
                }
                average = (double)sum / num;

                Console.WriteLine("The sum of the elements is: {0}", sum);
                Console.WriteLine("The average of the elements is: {0}", average);
        }


        public static void Main() {
                int[] array = new int[10];
                int n = 0;
                // Fill the array
                Fill(array, ref n);
                // Display the array contents
                //Dump(array, n);
                //Reverse(array, n);
                Compute(array, n);
                // Pause until user is done
                WaitForKey();
        }
}
